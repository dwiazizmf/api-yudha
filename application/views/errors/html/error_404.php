<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
 	$data['response'] = array( 'status' => FALSE, 'message' => 'Halaman tidak di temukan'); 
    $data['metadata'] = array('message' => FALSE, "code" => 404 );

    echo json_encode($data);
?>