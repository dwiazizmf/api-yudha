
<?php
    //$data_tt = json_decode($_POST['data_tt']);
    function penyebut($nilai) { 
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
 
    function terbilang($nilai) {
        if($nilai<0) { 
            $hasil = "minus ". trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }           
        return $hasil;
    }
?>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

<!------ Include the above in your HEAD tag ---------->
<style type="text/css">
    body {
        font-family: "Times New Roman"!important;
        font-size: 20;
    }

    .invoice-title h2, .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table > tbody > tr > .thick-line {
        border-top: 3px solid black;
    }
    
    .no_padding_left {
        padding-left:0px;
        padding-bottom:1px;
        font-size:12px;
    }

    .table-bordered th,
    .table-bordered td {
      border: 1px solid #000 !important;
    }
    
    .border_bottom {
        border: 1px solid black;
        height: 40;
        width: 618px;
    }
    
    .font_size_10{
        font-size: 12px;
    }
    
    .p_style{
        font-size: 12px;
        margin-bottom: 0px;
    }

    .table>tbody>tr>td {
        padding: 3px!important;
    }
</style>
<div class="">   
   <?php
    $i = 0;
    $j = 1;
    $line = 0;
    $subTotal = 0;
    $total_jumlah = 0;
    $total_volum = 0;
    $total_tonase = 0;
    $tgl_terima = '';
    $no_conote = '';
    $term = '';
    $pengirim = '';
    $telp = '';
    $no_container = '';
    $no_segel = '';
    $size_container = '';
    $penerima = '';
    $bln_txt = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember"); 
    $packageDesc = '';
    $pt = '';
    $halaman = 1;
   ?>
    <div class="row">
        <div class="col-xs-12"> 
            <!--<div class="row">
                <div class="col-xs-6 text-left">
                    <label>Printed On : <?php echo date('j')." ".$bln_txt[date('n')]." ".date('Y'); ?></label>
                </div>
                <div class="col-xs-6 text-right">
                    <label>Page 1 of 1</label>
                </div>
            </div>-->
            <?php 
                $arr_data = [];
                $arr_data_new = [];
                $arr_data_new_new = [];
                $total_sejumlah = 0;
                $arr_name = [];
                $arr_name_satuan = [];
                $desk_name = '';
                $satuan_name = '';
                $total_jumlah_satuan = 0;
                $total_volum_satuan = 0;
                $total_tonase_satuan = 0;  
                $bln_txt = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember"); 
                $ceil_40 = 0;

                foreach ($data_tt as $hasil) {   
                    if ($i == 0) {
                        $tgl_terima = date('j',strtotime($hasil->ConnoteDate))." ".$bln_txt[date('n',strtotime($hasil->ConnoteDate))]." ".date('Y',strtotime($hasil->ConnoteDate));
                        $no_conote = $hasil->ConnoteNumber;
                        $term = $hasil->ServiceTypeCode;
                        $pengirim = $hasil->ShipFromName.'</br>'.$hasil->Address1;
                        $telp = $hasil->ContactPhone;
                        $no_container = $hasil->ContainerNumber;
                        $no_segel = $hasil->SealNumber;
                        $size_container = $hasil->CtName;
                        $penerima = $hasil->consigneeName."-(".$hasil->CustomerProjectNumber.")</br>".$hasil->consigneeAddress;
                        $packageDesc = $hasil->PackageDescription;
                        $pt = $hasil->pt;
                    }

                    $total_jumlah = $total_jumlah + floatval($hasil->QTY);
                    $total_volum = $total_volum + floatval($hasil->WeightVol);
                    $total_tonase = $total_tonase + floatval($hasil->Weight);

                   
                    if (($desk_name != $hasil->ItemName."_".$hasil->UOM)) {
                        if (!in_array($hasil->ItemName."_".$hasil->UOM, $arr_name)){
                            $arr_name[] = $hasil->ItemName.'_'.$hasil->UOM;
                            $arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0] = array('satuan' => 0, 'volume' => 0, 'tonase' => 0);
                        }
                    }


                    $total_jumlah_satuan = $arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0]['satuan'] + floatval($hasil->QTY);
                    //echo $total_jumlah_satuan."=".$arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0]['satuan']." + ".floatval($hasil->QTY)."</br>";
                    $total_volum_satuan = $arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0]['volume'] + floatval($hasil->WeightVol);
                    //echo $total_volum_satuan."=".$arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0]['volume']." + ".floatval($hasil->WeightVol)."</br>";
                    $total_tonase_satuan = $arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0]['tonase'] + floatval($hasil->Weight);  
                    //echo $total_tonase_satuan."=".$arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0]['tonase']." + ".floatval($hasil->Weight)."</br>";
                    $arr_name_satuan[$hasil->ItemName.'_'.$hasil->UOM][0] = array('satuan' => $total_jumlah_satuan, 'volume' => $total_volum_satuan, 'tonase' => $total_tonase_satuan);

                    $count_43 = strlen($hasil->ItemName);
                    $ceil_40 = (($count_43/43) < 1)?$ceil_40+0:$ceil_40 + ceil($count_43/43);


                    $arr_data[$hasil->ItemName.'_'.$hasil->UOM][0] = array('jumlah' => $total_jumlah_satuan, 'satuan' => $hasil->UOM, 'deskripsi' => $hasil->ItemName, 'ukuran' => '', 'volume' => $total_volum_satuan, 'tonase' => $total_tonase_satuan);
                    if($hasil->ukuran != ''){
                        $arr_data[$hasil->ItemName.'_'.$hasil->UOM][$i+1] = array('jumlah' => '', 'satuan' => '', 'deskripsi' => '', 'ukuran' => $hasil->ukuran, 'volume' => '', 'tonase' => '' );
                    }

                    $desk_name = $hasil->ItemName."_".$hasil->UOM;
                    $satuan_name = $hasil->UOM;
                   
                    $i++;
                }

                $okk = 0;
                for ($z=0; $z < count($arr_name); $z++) { 
                    $ok = 0;
                   
                    foreach ($arr_data[$arr_name[$z]] as $hasile) {
                        if ($ok == 1) {
                            $okk = $okk - 1;
                            $data_okk = $arr_data_new[$okk];
                            $arr_data_new[$okk] = array('jumlah' => $data_okk['jumlah'], 'satuan' => $data_okk['satuan'], 'deskripsi' => $data_okk['deskripsi'], 'ukuran' => $hasile['ukuran'], 'volume' => $data_okk['volume'], 'tonase' => $data_okk['tonase'] );
                        }else{
                            $arr_data_new[$okk] = array('jumlah' => $hasile['jumlah'], 'satuan' => $hasile['satuan'], 'deskripsi' => $hasile['deskripsi'], 'ukuran' => $hasile['ukuran'], 'volume' => $hasile['volume'], 'tonase' => $hasile['tonase'] );    
                        }
                        
                        $ok++;
                        $okk++;
                    }
                }

                // for ($zz=0; $zz < 45; $zz++) { 
                //     $arr_data_new[] = array('jumlah' => '', 'satuan' => '', 'deskripsi' => '', 'ukuran' => '34 x 36 x 48 x 2', 'volume' => '', 'tonase' => '' );
                // }

                $jml_data = count($arr_data_new) + $ceil_40;
                $sisa_tambah = [];
                if ($jml_data > 9 && $jml_data <= 12) {
                    //var_dump($jml_data);
                    if ($jml_data == 12) {
                        $sisa_tambah[] = 0;
                        $sisa_tambah[] = 1; 
                    }else{
                        $sisa_tambah[] = 0;
                        $sisa_tambah[] = 12 - $jml_data;    
                    }
                    $sisa_tambah[] = 'atas';
                }elseif ($jml_data > 12) {
                    $aa = ($jml_data - 12) % 19;
                    //var_dump((19-$aa));
                    if ($aa == 18) {
                        $sisa_tambah[] = 1;
                        $sisa_tambah[] = 19 - $aa;
                    }else{
                        if ($aa > 11) {
                            if ((19-$aa) >= 7) {
                                $sisa_tambah[] = 1;
                                $sisa_tambah[] = 0;
                            }else{
                                $sisa_tambah[] = 1;
                                $sisa_tambah[] = 19 - $aa;    
                            }
                        }else{
                            if ($aa == 0) {
                                $sisa_tambah[] = 1;
                                $sisa_tambah[] = 1;
                            }else{
                                $sisa_tambah[] = 1;
                                $sisa_tambah[] = 0;
                            }
                        }
                    }

                    if ((19-$aa) > 2 && (19-$aa) <= 6) {
                        //$sisa_tambah[0] = 0;
                        $sisa_tambah[] = 'bawah';
                    }else{
                        $sisa_tambah[] = 'atas';    
                    }  
                }else{
                    if ((12-$jml_data) <= 6) {
                        $sisa_tambah[] = 0;
                        $sisa_tambah[] = 1;
                        $sisa_tambah[] = 'bawah';
                    }else{
                        $sisa_tambah[] = 0;
                        $sisa_tambah[] = 0;
                        $sisa_tambah[] = 'atas';
                    }
                }


                //var_dump($sisa_tambah);
            ?>
            
            
            <div class="text-right"><label style="font-size: 10px; margin-right: 10px;">hal <?php echo $halaman;?></label></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-12 text-left"><strong>&nbsp;</strong></div>   
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 text-center"><strong><h4>&nbsp;</h4></strong></div>
                </div>
            </div>
                     
            <div class="row">
                <div class="col-xs-6">
                    <div class="col-xs-12">
                        <div class="col-xs-3 text-left no_padding_left" style="width: 20.5%;">Tgl Terima</div>
                        <div class="col-xs-1 text-left no_padding_left" style="padding:0px;width: 1%;margin-right: 10px;">:</div>
                        <div class="col-xs-8 text-left no_padding_left"> <?php echo $tgl_terima; ?> <label id="jam_now" style="font-weight: 100;"></label></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-3 text-left no_padding_left" style="width: 20.5%;">No Connote</div>
                        <div class="col-xs-1 text-left no_padding_left" style="padding:0px;width: 1%; margin-right: 10px;">:</div>
                        <div class="col-xs-8 text-left no_padding_left"> <?php echo $no_conote; ?></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-3 text-left no_padding_left" style="width: 20.5%;">Term</div>
                        <div class="col-xs-1 text-left font_size_10 no_padding_left" style="padding:0px;width: 1%; margin-right: 10px;">:</div>
                        <div class="col-xs-8 text-left no_padding_left"> <?php echo $term; ?></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-3 text-left no_padding_left" style="width: 20.5%;">Pengirim</div>
                        <div class="col-xs-1 text-left no_padding_left" style="padding:0px;width: 1%;font-size: 9px; ">:</div>
                        
                    </div> 
                    <div class="col-xs-12">
                        <div class="col-xs-12 text-left no_padding_left" style="margin-bottom: 2px; "> 
                            <?php echo $pengirim; ?> 
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-3 text-left no_padding_left" style="width: 20.5%;">Telp</div>
                        <div class="col-xs-1 text-left no_padding_left" style="padding:0px;width: 1%; margin-right: 10px;">:</div>
                        <div class="col-xs-8 text-left no_padding_left"> <?php echo $telp; ?></div>
                    </div>
                </div>
                <div class="col-xs-6" style="padding-left: 0px;padding-right: 0px;">
                    <div class="col-xs-12">
                        <div class="col-xs-4 text-left no_padding_left" style="width: 22.1%;">No Container</div>
                        <div class="col-xs-1 text-left no_padding_left" style="padding:0px;width: 1%; margin-right: 10px;">:</div>
                        <div class="col-xs-7 text-left no_padding_left"> <?php echo $no_container; ?></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 text-left no_padding_left" style="width: 22.1%;">No Segel</div>
                        <div class="col-xs-1 text-left no_padding_left" style="padding:0px;width: 1%; margin-right: 10px;">:</div>
                        <div class="col-xs-7 text-left no_padding_left"> <?php echo $no_segel; ?></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 text-left no_padding_left" style="width: 22.1%;">Size Container</div>
                        <div class="col-xs-1 text-left font_size_10 no_padding_left" style="padding:0px;width: 1%; margin-right: 10px;">:</div>
                        <div class="col-xs-7 text-left no_padding_left"> <?php echo $size_container; ?></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 text-left no_padding_left" style="width: 22.1%;">Penerima</div>
                        <div class="col-xs-1 text-left no_padding_left" style="padding:0px;width: 1%;font-size: 9px;">:</div>
                        
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-12 text-left no_padding_left" style="margin-bottom: 2px; "> 
                            <?php echo $penerima; ?> 
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel" style="margin-bottom: 0px;">
                <!-- <div class="panel-heading">
                    <h5 class="panel-title "><strong style="font-size: 13; padding-top: 10px; padding-bottom: 10px;">TELAH DITERIMA NOTA TAGIHAN BESERTA SURAT JALAN / BUKTI PENERIMAAN BARANG DENGAN PERINCIAN SEBAGAI BERIKUT : </strong></h5>
                </div> -->
                <div class="panel-body" style="padding-top: 10px; padding-bottom: 0px;">
                    <div class="">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center"><strong style="font-size: 12;">JUMLAH</strong></th>
                                    <th class="text-center"><strong style="font-size: 12;">DESKRIPSI BARANG/CARGO</strong></th>
                                    <th class="text-center"><strong style="font-size: 12;">UKURAN</strong></th>
                                    <th class="text-center"><strong style="font-size: 12;">VOLUME/M3</strong></th>
                                    <th class="text-center"><strong style="font-size: 12;">VOLUME TONASE</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                     //var_dump($arr_name[$ii]);
                                    $abc = 1;
                                    $bagihasil = 13;
                                    $bagibagi = 0;
                                    foreach ($arr_data_new as $hasil_isi) { 

                                ?>
                                    <tr>
                                        <td class="item text-right" style="font-size: 12;"><?php echo $hasil_isi['jumlah'].' '.$hasil_isi['satuan']; ?></td>
                                        <td class="item text-left" style="font-size: 12;">
                                            <?php
                                                $nama_item_len = strlen($hasil_isi['deskripsi']);
                                                if($nama_item_len > 43){
                                                    $ceil_40 = 0;
                                                    $ceil_total = ceil($nama_item_len/43);
                                                    for($i=0; $i < $ceil_total; $i++){
                                                        if($i == ($ceil_total-1)){
                                                            echo substr($hasil_isi['deskripsi'],$ceil_40,43);
                                                        }else{
                                                            echo substr($hasil_isi['deskripsi'],$ceil_40,43)."</br>";
                                                        }
                                                        
                                                        //echo $ceil_40."</br>";
                                                        $ceil_40 = $ceil_40 + 43;
                                                        
                                                    }
                                                }else{
                                                    echo $hasil_isi['deskripsi'];
                                                } 
                                            ?>
                                        </td>
                                        <td class="item text-center" style="font-size: 12;">
                                            <?php echo $hasil_isi['ukuran']; ?>
                                        </td>
                                        <td class="text-right" style="font-size: 12;">
                                           <?php echo ($hasil_isi['volume'] > 0)?number_format((float)$hasil_isi['volume'], 3, '.', ''):$hasil_isi['volume']; ?>
                                        </td>
                                        <td class="text-right" style="font-size: 12;"> <?php echo ($hasil_isi['tonase'] > 0)?number_format((float)$hasil_isi['tonase'], 3, '.', ''):$hasil_isi['tonase']; ?> </td>
                                    </tr>
                                <?php 
                                        if ($sisa_tambah[0]) {
                                            if (($abc%$bagihasil) == 0 && $bagibagi < 1) {
                                                $abc = 1;
                                                $bagibagi++;
                                                $bagihasil = 20; 
                                                $halaman++;
                                ?>
                                                </tbody>
                                                </table>
                                                <div class="text-right"><label style="font-size: 10px; margin-right: 10px; margin-bottom: 70px;">hal <?php echo $halaman;?></label></div>
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center"><strong style="font-size: 12;">JUMLAH</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">DESKRIPSI BARANG/CARGO</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">UKURAN</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">VOLUME/M3</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">VOLUME TONASE</strong></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>  
                                <?php
                                            }elseif (($abc%$bagihasil) == 0 && $bagibagi > 0) {
                                                $bagibagi++;
                                                $abc = 1;
                                                $halaman++;
                                ?>
                                                </tbody>
                                                </table>
                                                <div class="text-right"><label style="font-size: 10px; margin-right: 10px; margin-bottom: 70px;">hal <?php echo $halaman;?></label></div>
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center"><strong style="font-size: 12;">JUMLAH</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">DESKRIPSI BARANG/CARGO</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">UKURAN</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">VOLUME/M3</strong></th>
                                                            <th class="text-center"><strong style="font-size: 12;">VOLUME TONASE</strong></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>  
                                <?php
                                            }
                                        }

                                        $abc++;
                                    } 
                                ?>                                  


                                <tr>
                                    <td class="no-line text-right" style="font-size: 12;"> 
                                        
                                         <?php echo $total_jumlah; ?> KOLI
                                        
                                    </td>
                                    <td colspan="2" class="no-line text-right" style="font-size: 12;"><strong >** Total **</strong></td>
                                    <td class="no-line text-right" style="font-size: 12;"> 
                                        
                                         <?php echo ($total_volum > 0)?number_format((float)$total_volum, 3, '.', ''):$total_volum; ?>
                                        
                                    </td>
                                    <td class="no-line text-right" style="font-size: 12;"> 
                                       
                                         <?php echo ($total_tonase > 0)?number_format((float)$total_tonase, 3, '.', ''):$total_tonase; ?>
                                        
                                    </td>
                                </tr> 

                                <?php
                                $iCount = $sisa_tambah[1]-1;
                                //var_dump($iCount);
                                if (($sisa_tambah[1]) <= 6) {
                                    $iCount = $iCount - 2;
                                }
                               
                                ?>
                            </tbody>
                        </table>
                        <?php 
                            if ($sisa_tambah[1] > 0 && $sisa_tambah[2] == 'atas') {
                                $halaman++;
                                echo '<div class="text-right"><label style="font-size: 10px; margin-right: 10px; margin-bottom: 70px;">hal '.$halaman.'</label></div>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <!-- <div class="col-xs-12">
                <div class="col-xs-3 text-left no_padding_left">Status</div>
                <div class="col-xs-1 text-left" style="padding:0px;width: 1%;">:</div>
                <div class="col-xs-8 text-left no_padding_left"> Belum Lunas</div> 
            </div>
            <div class="col-xs-12" style="padding-top: 9px;">
                <div class="col-xs-3 text-left no_padding_left">Jatuh Tempo</div>
                <div class="col-xs-1 text-left" style="padding:0px;width: 1%;">:</div>
                <div class="col-xs-8 text-left no_padding_left"> 20 Septembr 2019</div> 
            </div> -->
            <div class="col-xs-12">
                <div class="col-xs-9 text-left no_padding_left" style="margin-bottom: 0px;">
                    <p style="text-align: left; font-size: 10" >
                        Note : <?php echo $packageDesc; ?> </br>
                        1. Pihak Ekspedisi tidak bertanggung jawab atas isi barang dalam dus atau kemasan dari kekurangan ataupun ketidak sesuain barang.</br>
                        2. Pihak Ekspedisi tidak menanggung kerugian yang timbul karena force majeur / bencana alam</br>
                        3. Pihak Ekspedisi tidang menanggung biaya asuransi pengiriman
                    </p>
                </div>
                <!-- <div class="col-xs-2 text-left font_size_10" style="padding:0px;">
                    </br></br>
                    <p></p>            
                </div> -->
            </div>
        </div>
        </div>
        <?php 
          for ($i=0; $i < $iCount; $i++) { 
                                ?>    
                                    <tr> 
                                        <td colspan="5" class="text-center" style="font-size: 12; border: 1px solid white!important;">&nbsp;</td>
                                    </tr>
                                <?php
                                    }
        
                            if ($sisa_tambah[1] > 0 && $sisa_tambah[2] == 'bawah') {
                                $halaman++;
                                echo '<div class="text-right"><label style="font-size: 10px; margin-right: 10px; margin-bottom: 70px;">hal '.$halaman.'</label></div>';
                            }
                        ?>
    </div>
    <div class="row">
        <div class="col-xs-12" style="padding-top: 5px;">
            <div class="col-xs-4 text-center"> 
                <p style="font-size: 12px;">Yang Menyerahkan</p>
            </div> 
             <div class="col-xs-4 text-center" style="height: 10px;">
                <div id="qrcode" style="width:100px; height:100px; margin-left:60px;"></div>
            </div> 
            <div class="col-xs-4 text-center" style="height: 10px;">
                <p style="font-size: 12px;">Yang Menerima</p>
            </div> 
        </div>
        <div class="col-xs-12" style="padding-top: 30px;">
            <div class="col-xs-4 text-center"> 
                <p style="font-size: 12px;">(.............................................)</p>
            </div> 
             <div class="col-xs-4 text-center" style="height: 10px;">
            </div> 
            <div class="col-xs-4 text-center" style="height: 10px;">
                <p style="font-size: 12px;"><?php echo $pt;?></p>
            </div> 
        </div>
    </div>
   <!--  <div class="row">
        <div class="col-xs-12" style="padding-top: 50px;">
            <div class="col-xs-6 text-center"> 
                <div class="col-xs-6 text-center"> 
                    <p style="font-size: 12px;">(</p>
                </div> 
                <div class="col-xs-6 text-center"> 
                    <p style="font-size: 12px;">)</p>
                </div>
            </div> 
            <div class="col-xs-6 text-center" style="height: 10px;">
                <div class="col-xs-6 text-center"> 
                    <p style="font-size: 12px;">(</p>
                </div>
                <div class="col-xs-6 text-center"> 
                    <p style="font-size: 12px;">)</p>
                </div>
            </div> 
        </div>
    </div> -->
    <!-- <div class="row">
        <div class="col-xs-12" style="padding-top: 20px;">
            <div class="col-xs-12 text-left"> 
                <p style="font-size: 12px;">*Note : Harap ditandatangan dilengkapi dengan tanda terima, stempel (jika ada), nama jelas dan dikembalikan kepada kami</p>
            </div> 
        </div>
    </div> -->
    <!--
    <div class="row">
        <div class="col-xs-12" >
            <div class="col-xs-10"> 
                <div class="col-xs-12" style="padding: 5px;">
                    <p class="p_style" >
                        Bank Account</br>
                        PT. Yudha Antar Nusa</br>
                        BCA Capem Jembatan Dua Raya</br>
                        Mandiri KCU Jakarta-Pusat
                    </p>
                </div>
            </div> 
        </div>
    </div>
    -->
</div>
<script type="text/javascript" src="../../../public/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../public/js/qrcode.js"></script>
<script type="text/javascript">
    var qrcode = new QRCode(document.getElementById("qrcode"), {
        width : 80,
        height : 80
    });

    function makeCode () {      
        var currentTime = new Date();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();

        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        $("#jam_now").text(hours+":"+minutes);

        qrcode.makeCode('<?php echo $no_conote; ?>');
    }

    makeCode();
    window.print();
</script>