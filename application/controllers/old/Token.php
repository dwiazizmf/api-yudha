<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/jwt/JWT.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Token extends REST_Controller {
	function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function index_post()
    {
     	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus GET'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    
    }

    function index_get() {
        $headers = $this->input->request_headers();
        
        if ( !isset($headers['x-username']) && !isset($headers['x-password']))
        {
        	//$data['response'] = array( 'status' => FALSE, 'message' => 'Username dan password harus diisi'); 
        	$data['metadata'] = array('message' => 'Username dan password harus diisi', "code" => 405 );
            $this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED); // NOT_FOUND (404) being the HTTP response code
        }else {
            $username = $headers['x-username'];
     	    $password = $headers['x-password'];

        	
            $id = 0;
            if($username == 'admin' && $password == 'bpjs'){
                $id = 1;
            }

            // Validate the id.
            if ($id <= 0)
            {
            	//$data['response'] = array( 'status' => FALSE, 'message' => 'User tidak di temukan'); 
        		$data['metadata'] = array('message' => 'Username atau Password Tidak Sesuai', "code" => 401 );
                // Invalid id, set the response and exit.
                $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // BAD_REQUEST (400) being the HTTP response code
            }else{
            	$key = JWT::key_encode_decode();
				$payload = array(
				    "username" => $username,
				    "password" => $password,
				    "login_time" => date('Y-m-d H:i:s.u'),
				    "login_unix" => strtotime('now')
				);

				$jwt = JWT::encode(json_encode($payload), $key);

            	$data['response'] = array('token' => $jwt); 
        		$data['metadata'] = array('message' => "OK", "code" => 200 );
            	$this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    }

    function index_put() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_delete() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
}
