<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/jwt/JWT.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Status_antrean extends REST_Controller {
    function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->database();
    }

    public function index_post()
    {
        $headers = $this->input->request_headers();
        
        if ((!isset($headers['X-Token']) && !isset($headers['x-token'])) || !isset($headers['x-username'])) {
            //$data['response'] = array( 'status' => FALSE, 'message' => 'tidak ada token header'); 
            $data['metadata'] = array('message' => 'header token atau username kosong', "code" => 405 );
            $this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED); // NOT_FOUND (404) being the HTTP response code
        }else{
            $token = (isset($headers['X-Token']))?$headers['X-Token']:$headers['x-token'];
            $decoded = $this->decode_jwt($token);
            $json_decoded = json_decode($decoded);
            $array_decoded = (array)$json_decoded;
            $username_jwt = $array_decoded['username'];
        
            $username = $headers['x-username'];
            if ($username != $username_jwt) {
                //$data['response'] = array( 'status' => FALSE, 'message' => 'token expired'); 
                $data['metadata'] = array('message' => 'header token atau header username salah', "code" => 401 );
                $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // NOT_FOUND (404) being the HTTP response code
            }else{
                if(!$this->cek_tanggal($this->post("tanggalperiksa"))){
                        $data['metadata'] = array('message' => 'Format tanggal periksa tidak sesuai', "code" => 200 );
                        $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                }else{
                    $to_time = strtotime('now');
                    $from_time = $array_decoded['login_unix'];
                    $minutes_auth = round(abs($to_time - $from_time) / 60,2);
                    if ($minutes_auth > 60) {
                        //$data['response'] = array( 'status' => FALSE, 'message' => 'token expired'); 
                        $data['metadata'] = array('message' => 'Token Expired', "code" => 401 );
                        $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // NOT_FOUND (404) being the HTTP response code
                    }else{
                        $value = $this->post();
                        $this->db->where("kd_unit", $value["kodepoli"]);
                        $this->db->select('nama_unit');
                        $poli_ol = $this->db->get('unit_ol')->result();
                        $jml_poli_ol = count($poli_ol);
                        if($jml_poli_ol < 1){
                            $data['metadata'] = array('message' => 'Data poli tidak di temukan', "code" => 200 );
                            $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                        }else{
                            $this->db->where("kd_dokter", $value["kodedokter"]);
                            $this->db->select('nama_dokter');
                            $dokter_ol = $this->db->get('dokter_ol')->result();
                            $jml_dokter_ol = count($dokter_ol);
                            if($jml_dokter_ol < 1){
                                $data['metadata'] = array('message' => 'Data dokter tidak di temukan', "code" => 200 );
                                $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                            }else{
                                $this->db->where('p.tanggalperiksa', $this->post("tanggalperiksa"));
                                $this->db->where('p.kodepoli', $this->post("kodepoli"));
                                $this->db->where('p.kodedokter', $this->post("kodedokter"));
                                $this->db->order_by('p.tanggalperiksa', 'DESC');
                                $this->db->select("p.namapoli, p.namadokter, s.totalantrean, s.sisaantrean, '' as antreanpanggil, s.sisakuotajkn, s.kuotajkn, s.sisakuotanonjkn, s.kuotanonjkn, p.keterangan ");
                                $this->db->from('pasien_daftar p');
                                $this->db->join('status_antrean s', 's.kodepoli = p.kodepoli');
                                $kontak = $this->db->get()->result();
                            
                                $id = count($kontak);
                            
                                // $namapoli = 'namapoli';
                                // $kontak = array(
                                //     "namapoli" => "Anak",
                                //     "namadokter" => "Dr. Hendra",
                                //     "totalantrean" => 25,
                                //     "sisaantrean" => 4,
                                //     "antreanpanggil" => "A-21",
                                //     "sisakuotajkn" => 5,
                                //     "kuotajkn" => 30,
                                //     "sisakuotanonjkn" => 5,
                                //     "kuotanonjkn" => 30,
                                //     "keterangan" => ""
                                // );
                                
                                if ($id <= 0)
                                {
                                    //$data['response'] = array( 'status' => FALSE, 'message' => 'Tidak ada data'); 
                                    $data['metadata'] = array('message' => 'Maaf tidak ada jadwal ditanggal '.$this->post("tanggalperiksa"), "code" => 200 );
                                    $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                                }else{
                                    $data['response'] = $kontak; 
                                    $data['metadata'] = array('message' => "OK", "code" => 200 );
                                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function cek_tanggal($date)
    {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            return true;
        } else {
            return false;
        }
    }

    private function encode_jwt($payload)
    {
        $key = JWT::key_encode_decode();
        $jwt = JWT::encode($payload, $key);
        return $jwt;
    }

    private function decode_jwt($jwt)
    {
        $key = JWT::key_encode_decode();
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        return $decoded;
    }

    function index_get() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_put() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_delete() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
}
