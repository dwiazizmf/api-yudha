<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/jwt/JWT.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Tambah_antrean extends REST_Controller {
    function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->database();
    }

    public function index_post()
    {
        $headers = $this->input->request_headers();
        
        if (!isset($headers['X-Token']) && !isset($headers['x-token'])) {
            //$data['response'] = array( 'status' => FALSE, 'message' => 'tidak ada token header'); 
            $data['metadata'] = array('message' => 'tidak ada token header', "code" => 405 );
            $this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED); // NOT_FOUND (404) being the HTTP response code
        }else{
            $token = (isset($headers['X-Token']))?$headers['X-Token']:$headers['x-token'];
            $decoded = $this->decode_jwt($token);
            $json_decoded = json_decode($decoded);
            $array_decoded = (array)$json_decoded;
            //var_dump($array_decoded);
            $to_time = strtotime('now');
            $from_time = $array_decoded['login_unix'];
            $minutes_auth = round(abs($to_time - $from_time) / 60,2);
            if ($minutes_auth > 60) {
                //$data['response'] = array( 'status' => FALSE, 'message' => 'token expired'); 
                $data['metadata'] = array('message' => 'token expired', "code" => 401 );
                $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // NOT_FOUND (404) being the HTTP response code
            }else{
                $value = $this->post();
                $this->db->where("kodebooking", $value["kodebooking"]);
                $this->db->where("tanggalperiksa", $value["tanggalperiksa"]);
                $this->db->select('nomorantrian, checkIn');
                $pasien_daftar = $this->db->get('pasien_daftar')->result();
                //var_dump($pasien_daftar);
                if(count($pasien_daftar) > 0){
                    $dataPasienDaftar = $pasien_daftar[0];
                    $data['response'] = array( 'status' => FALSE, 'message' => 'Anda sudah terdaftar untuk pemeriksaan pada hari '.$value['tanggalperiksa'].' dan sudah berada di no antrian '.$dataPasienDaftar->nomorantrian.', silahkan checkin jika belum melakukan checkin'); 
                    $data['metadata'] = array('message' => FALSE, "code" => 200 );
                    $this->set_response($data, REST_Controller::HTTP_OK );
                    //$this->insertPendaftaran($value); 
                }else{
                    $bln_txt = array(1=>"senin","selasa","rabu", "kamis", "jumat","sabtu","minggu");
                    $bln_now = $bln_txt[date('N',strtotime($value['tanggalperiksa']))];
                    $this->db->where("kd_dokter", $value["kodedokter"]);
                    $this->db->where("(".$bln_now." != '-' AND ".$bln_now." != '00:00-00:00')");
                    $this->db->select('nid');
                    $pasien_daftar = $this->db->get('jadwal_dokter_ol')->result();    
                    if(count($pasien_daftar) > 0){
                        $this->db->where("nik", $value["nik"]);
                        $this->db->select('nid');
                        $kontak = $this->db->get('pasien_ol')->result();
        
                        $id = count($kontak);
                        
                        if ($id < 1)
                        {
                            $norm = time();
                    
                            $data_ = array(
                                    "norm" => $norm,
                                    "nomorkartu" => $value["nomorkartu"],
                                    "nik" => $value["nik"],
                                    "telepon" => $value["nohp"],
                                );
                            $insert = $this->db->insert('pasien_ol', $data_);
                            //$insert = 1;
                            if (!$insert) {
                                $data['response'] = array( 'status' => FALSE, 'message' => 'Gagal tambah data'); 
                                $data['metadata'] = array('message' => FALSE, "code" => 200 );
                                $this->set_response($data, REST_Controller::HTTP_OK );
                            }else{
                                $this->insertPendaftaran($value); 
                            } 
                        }else{
                            $this->insertPendaftaran($value); 
                        }
                    }else{
                        $data['response'] = array( 'status' => FALSE, 'message' => 'Tidak ada jadwal dokter pada tanggal ini'); 
                        $data['metadata'] = array('message' => FALSE, "code" => 200 );
                        $this->set_response($data, REST_Controller::HTTP_OK );
                    }
                }
            }
        }
    }
    
    private function insertPendaftaran($value){
        // $this->db->where("kd_booking", $value["kodebooking"]);
        // $this->db->select('no_antrian, tgl_booking, tgl_daftar');
        // $pasien_daftar = $this->db->get('pendaftaran_ol')->result();
        // //var_dump($pasien_daftar);
        // if(count($pasien_daftar) > 0){
        //     $dataPasienDaftar = $pasien_daftar[0];
        //     $data['response'] = array( 'status' => FALSE, 'message' => 'Anda sudah melakukan booking untuk tanggal '.$dataPasienDaftar->tgl_booking.' silahkan proses ambil no antrian'); 
        //     $data['metadata'] = array('message' => FALSE, "code" => 200 );
        //     $this->set_response($data, REST_Controller::HTTP_OK );
        // }
        
        $this->db->where("kodepoli", $value["kodepoli"]);
        $this->db->where("kodedokter", $value["kodedokter"]);
        $this->db->where("tanggalperiksa", $value["tanggalperiksa"]);
        $this->db->select('id, totalantrean, sisaantrean, kuotanonjkn, sisakuotanonjkn');
        $statusAntrean = $this->db->get('status_antrean')->result();
        
        if(count($statusAntrean) > 0){
            $data_status_antrean = $statusAntrean[0];
            $this->db->where('id', $data_status_antrean->id);
            $this->db->update('status_antrean', array('sisaantrean' => $value["sisakuotanonjkn"], 'sisakuotanonjkn' => $value["sisakuotanonjkn"]));
        }
        
        $pendaftaran_ol = array(
            'tgl_booking'           => $value["tanggalperiksa"],
            'tgl_daftar'           => $value["tanggalperiksa"],
            'nik' => $value['nik'],
            'kd_unit' => $value['kodepoli'],
            'kd_booking' => $value['kodebooking'],
            'no_antrian' => $value['angkaantrean'],
        );
        $insert = $this->db->insert('pendaftaran_ol', $pendaftaran_ol);
                    
        $data_ = array(
            "nopendaftaran"=> time(),
            "kodebooking"=> $value["kodebooking"],
            "nomorkartu"=> $value["nomorkartu"],
            "nik"=> $value["nik"],
            "notelp"=> $value["nohp"],
            "kodepoli"=> $value["kodepoli"],
            "namapoli"=> $value["namapoli"],
            "norm"=> $value["norm"],
            "tanggalperiksa"=> $value["tanggalperiksa"],
            "kodedokter"=> $value["kodedokter"],
            "namadokter"=> $value["namadokter"],
            "jenisantrian"=> $value["jeniskunjungan"],
            "nomorreferensi"=> $value["nomorreferensi"],
            "nomorantrian"=> $value["angkaantrean"],
            "estimasidilayani"=> $value["estimasidilayani"],
            "keterangan" => $value["keterangan"],
            );
        $insert = $this->db->insert('pasien_daftar', $data_);
        //$insert = 1;
        if ($insert) {
            //$data['response'] = array( 'norm' => TRUE); 
            $data['metadata'] = array('message' => "OK", "code" => 200 );
            //$this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            $this->response($data, 200);
        } else {
            $data['response'] = array( 'status' => FALSE, 'message' => 'Gagal tambah data'); 
            $data['metadata'] = array('message' => FALSE, "code" => 200 );
            $this->set_response($data, REST_Controller::HTTP_OK );
        }
    }

    private function encode_jwt($payload)
    {
        $key = JWT::key_encode_decode();
        $jwt = JWT::encode($payload, $key);
        return $jwt;
    }

    private function decode_jwt($jwt)
    {
        $key = JWT::key_encode_decode();
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        return $decoded;
    }

    function index_get() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_put() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_delete() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
}