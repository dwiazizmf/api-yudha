<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/jwt/JWT.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Pasien_baru extends REST_Controller {
    function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->database();
    }

    public function index_post()
    {
        $headers = $this->input->request_headers();
        
        if ((!isset($headers['X-Token']) && !isset($headers['x-token'])) || !isset($headers['x-username'])) {
            //$data['response'] = array( 'status' => FALSE, 'message' => 'tidak ada token header'); 
            $data['metadata'] = array('message' => 'header token atau username kosong', "code" => 405 );
            $this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED); // NOT_FOUND (404) being the HTTP response code
        }else{
            $token = (isset($headers['X-Token']))?$headers['X-Token']:$headers['x-token'];
            $decoded = $this->decode_jwt($token);
            $json_decoded = json_decode($decoded);
            $array_decoded = (array)$json_decoded;
            $username_jwt = $array_decoded['username'];
        
            $username = $headers['x-username'];
            if ($username != $username_jwt) {
                //$data['response'] = array( 'status' => FALSE, 'message' => 'token expired'); 
                $data['metadata'] = array('message' => 'header token atau header username salah', "code" => 401 );
                $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // NOT_FOUND (404) being the HTTP response code
            }else{
                $to_time = strtotime('now');
                $from_time = $array_decoded['login_unix'];
                $minutes_auth = round(abs($to_time - $from_time) / 60,2);
                if ($minutes_auth > 60) {
                    //$data['response'] = array( 'status' => FALSE, 'message' => 'token expired'); 
                    $data['metadata'] = array('message' => 'Token Expired', "code" => 401 );
                    $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // NOT_FOUND (404) being the HTTP response code
                }else{    
                    $value = $this->post();
                    //var_dump($this->post());
                    $this->db->where("nik", $value["nik"]);
                    $this->db->select('nid');
                    $kontak = $this->db->get('pasien_ol')->result();
    
                    $id = count($kontak);
    
                    if ($id < 1)
                    {
                        $norm = time();
                
                        $data_ = array(
                                "norm" => $norm,
                                "nomorkartu" => $value["nomorkartu"],
                                "nik" => $value["nik"],
                                "nomorkk" => $value["nomorkk"],
                                "nama_pasien" => $value["nama"],
                                "jenis_kelamin" => $value["jeniskelamin"],
                                "tanggallahir" => $value["tanggallahir"],
                                "telepon" => $value["nohp"],
                                "alamat" => $value["alamat"],
                                "kodeprop" => $value["kodeprop"],
                                "namaprop" => $value["namaprop"],
                                "kodedati2" => $value["kodedati2"],
                                "namadati2" => $value["namadati2"],
                                "kodekec" => $value["kodekec"],
                                "namakec" => $value["namakec"],
                                "kodekel" => $value["kodekel"],
                                "namakel" => $value["namakel"],
                                "rw" => $value["rw"],
                                "rt" => $value["rt"]
                            );
                        $insert = $this->db->insert('pasien_ol', $data_);
                        //$insert = 1;
                        if ($insert) {
                            $data['response'] = array( 'norm' => $norm); 
                            $data['metadata'] = array('message' => "Harap datang ke admisi untuk melengkapi data rekam medis", "code" => 200 );
                            //$this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                            $this->response($data, 200);
                        } else {
                            $data['response'] = array( 'status' => FALSE, 'message' => 'Gagal tambah data'); 
                            $data['metadata'] = array('message' => FALSE, "code" => 200 );
                            $this->set_response($data, REST_Controller::HTTP_OK );
                        }
                    }else{
                        $data['response'] = array( 'status' => FALSE, 'message' => 'Data pasien sudah ada'); 
                        $data['metadata'] = array('message' => FALSE, "code" => 200 );
                        $this->set_response($data, REST_Controller::HTTP_OK );
                    }
                }
            }
        }
    }

    private function encode_jwt($payload)
    {
        $key = JWT::key_encode_decode();
        $jwt = JWT::encode($payload, $key);
        return $jwt;
    }

    private function decode_jwt($jwt)
    {
        $key = JWT::key_encode_decode();
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        return $decoded;
    }

    function index_get() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_put() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_delete() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
}