<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/jwt/JWT.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Get_antrean extends REST_Controller {
    function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->database();
    }

    public function index_post()
    {
        $headers = $this->input->request_headers();
        
        if ((!isset($headers['X-Token']) && !isset($headers['x-token'])) || !isset($headers['x-username'])) {
            //$data['response'] = array( 'status' => FALSE, 'message' => 'tidak ada token header'); 
            $data['metadata'] = array('message' => 'header token atau username kosong', "code" => 405 );
            $this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED); // NOT_FOUND (404) being the HTTP response code
        }else{
            $token = (isset($headers['X-Token']))?$headers['X-Token']:$headers['x-token'];
            $decoded = $this->decode_jwt($token);
            $json_decoded = json_decode($decoded);
            $array_decoded = (array)$json_decoded;
            $username_jwt = $array_decoded['username'];
        
            $username = $headers['x-username'];
            if ($username != $username_jwt) {
                //$data['response'] = array( 'status' => FALSE, 'message' => 'token expired'); 
                $data['metadata'] = array('message' => 'header token atau header username salah', "code" => 401 );
                $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // NOT_FOUND (404) being the HTTP response code
            }else{
                $to_time = strtotime('now');
                $from_time = $array_decoded['login_unix'];
                $minutes_auth = round(abs($to_time - $from_time) / 60,2);
                if ($minutes_auth > 60) {
                    //$data['response'] = array( 'status' => FALSE, 'message' => 'token expired'); 
                    $data['metadata'] = array('message' => 'Token Expired', "code" => 401 );
                    $this->response($data, REST_Controller::HTTP_UNAUTHORIZED); // NOT_FOUND (404) being the HTTP response code
                }else{
                    if(!is_numeric($this->post("nomorkartu"))){
                        //$data['response'] = array( 'status' => FALSE, 'message' => 'Tidak ada data'); 
                        $data['metadata'] = array('message' => 'Nomor kartu harus mengandung angka saja.!!!', "code" => 200 );
                        $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                    }else{
                        $cek_tanggal = $this->cek_tanggal($this->post("tanggalperiksa")); 
                        if($cek_tanggal == 0 ){
                            $data['metadata'] = array('message' => 'Nomor Antrean Hanya Dapat Diambil 1 Kali Pada Tanggal Yang Sama', "code" => 200 );
                            $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                        }elseif($cek_tanggal == 2){
                            $data['metadata'] = array('message' => 'Format Tanggal Tidak Sesuai, format yang benar adalah yyyy-mm-dd', "code" => 200 );
                            $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                        }else{
                            $value = $this->post();
                            $kd_booking = time();
                            $this->db->where("nik", $value["nik"]);
                            $this->db->where('kd_unit', $this->post("kodepoli"));
                            $this->db->where("tgl_booking", $value["tanggalperiksa"]);
                            $this->db->select('no_antrian');
                            $pendaftaran_ol = $this->db->get('pendaftaran_ol')->result();
                            //var_dump($pasien_daftar);
                            if(count($pendaftaran_ol) > 0){
                                $dataPasienDaftar = $pendaftaran_ol[0];
                                $data['metadata'] = array('message' => 'Nomor Antrean Hanya Dapat Diambil 1 Kali Pada Tanggal Yang Sama', "code" => 200 );
                                $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                            }else{
                                $this->db->where("nik", $value["nik"]);
                                $this->db->where('kodepoli', $this->post("kodepoli"));
                                $this->db->where("tanggalperiksa", $value["tanggalperiksa"]);
                                $this->db->select('nomorantrian');
                                $pasien_daftar = $this->db->get('pasien_daftar')->result();
                                if(count($pasien_daftar) > 0){
                                    $dataPasienDaftar = $pasien_daftar[0];
                                    $data['metadata'] = array('message' => 'Nomor Antrean Hanya Dapat Diambil 1 Kali Pada Tanggal Yang Sama', "code" => 200 );
                                    $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                                }else{
                                    $this->db->where("tgl_booking", $value["tanggalperiksa"]);
                                    $this->db->where('kd_unit', $this->post("kodepoli"));
                                    $this->db->select('nid');
                                    $pendaftaran_ol = $this->db->get('pendaftaran_ol')->result();
                                    $jml_pendaftaran_ol = count($pendaftaran_ol);
                                    
                                    $this->db->where("tanggalperiksa", $value["tanggalperiksa"]);
                                    $this->db->where('kodepoli', $this->post("kodepoli"));
                                    $this->db->select('nopendaftaran');
                                    $pasien_daftar = $this->db->get('pasien_daftar')->result();
                                    $jml_pasien_daftar = count($pasien_daftar);
                                    
                                    $no_antrian = 1 + ($jml_pendaftaran_ol + $jml_pasien_daftar);
                                    
                                    $this->db->where("kd_dokter", $value["kodedokter"]);
                                    $this->db->select('nama_dokter');
                                    $dokter_ol = $this->db->get('dokter_ol')->result();
                                    $jml_dokter_ol = count($dokter_ol);
                                    if($jml_dokter_ol < 1){
                                        $data['metadata'] = array('message' => 'Data dokter tidak di temukan', "code" => 200 );
                                        $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                                    }else{
                                        $data_dokter_ol = $dokter_ol[0];
                                        
                                        $this->db->where("kd_unit", $value["kodepoli"]);
                                        $this->db->select('nama_unit');
                                        $poli_ol = $this->db->get('unit_ol')->result();
                                        $jml_poli_ol = count($poli_ol);
                                        if($jml_poli_ol < 1){
                                            $data['metadata'] = array('message' => 'Data poli tidak di temukan', "code" => 200 );
                                            $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                                        }else{
                                            $data_poli_ol = $poli_ol[0];
                                        
                                            $this->db->where("kodepoli", $value["kodepoli"]);
                                            $this->db->where("kodedokter", $value["kodedokter"]);
                                            $this->db->where("tanggalperiksa", $value["tanggalperiksa"]);
                                            $this->db->select('id, totalantrean, sisaantrean, kuotanonjkn, sisakuotanonjkn');
                                            $statusAntrean = $this->db->get('status_antrean')->result();
                                            //var_dump($statusAntrean);
                                            $id = count($statusAntrean);
                                            if($id > 0){
                                                $dataStatusAntrean = $statusAntrean[0];
                                                //var_dump($dataStatusAntrean->sisaantrean);
                                                $jml_sisa = $dataStatusAntrean->sisaantrean;
                                                if($jml_sisa == 0){
                                                    $data['metadata'] = array('message' => 'Kuota untuk hari ini sudah penuh', "code" => 200 );
                                                    $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                                                }else{
                                                    $sisaAntrean = 100 - ($jml_pendaftaran_ol + $jml_pasien_daftar + 1);
                                                    $this->db->where('id', $dataStatusAntrean->id);
                                                    $this->db->update('status_antrean', array('sisaantrean' => $sisaAntrean, 'sisakuotanonjkn' => $sisaAntrean));
                                                }
                                            }else{
                                                $data_antrean = array(
                                                    'kodepoli'           => $value['kodepoli'],
                                                     'namapoli'           => $data_poli_ol->nama_unit,
                                                     'kodedokter'           => (isset($value['kodedokter']))?$value['kodedokter']:'',
                                                     'namadokter'           => $data_dokter_ol->nama_dokter,
                                                     'tanggalperiksa'           => $value['tanggalperiksa'],
                                                     'jampraktek_start'           => '08:00',
                                                     'jamprakter_end'           => '16:00',
                                                     'totalantrean'           => 100,
                                                     'sisaantrean'           => 99 - $jml_pendaftaran_ol,
                                                     'antreanpanggil'           => 0,
                                                     'kuotajkn'           => 0,
                                                     'sisakuotajkn'           => 0,
                                                     'kuotanonjkn'           => 100,
                                                     'sisakuotanonjkn'           => 99 - $jml_pendaftaran_ol,
                                                     'keterangan'           => '',
                                                );
                                                $insert = $this->db->insert('status_antrean', $data_antrean);
                                            }
                                            
                                            $pendaftaran_ol = array(
                                                'tgl_booking'           => $value["tanggalperiksa"],
                                                'tgl_daftar'           => $value["tanggalperiksa"],
                                                'nik' => $value['nik'],
                                                'kd_unit' => $value['kodepoli'],
                                                'kd_booking' => $kd_booking,
                                                'no_antrian' => $no_antrian,
                                            );
                                            $insert = $this->db->insert('pendaftaran_ol', $pendaftaran_ol);
                                            
                                            $data_ = array(
                                                'nopendaftaran'           => $kd_booking,
                                                 'nomorkartu'           => $value['nomorkartu'],
                                                 'nik'           => $value['nik'],
                                                 'notelp'           => $value['nohp'],
                                                 'tanggalperiksa'           => $value['tanggalperiksa'],
                                                 'kodepoli'           => $value['kodepoli'],
                                                 'namapoli'           => $data_poli_ol->nama_unit,
                                                 'nomorreferensi'           => $value['nomorreferensi'],
                                                 'jenisreferensi'           => '',
                                                 'jenisrequest'           => '',
                                                 'polieksekutif'           => '',
                                                 'nomorantrian'           => $no_antrian,
                                                 'kodebooking'           => $kd_booking,
                                                 'jenisantrian'           => $value['jeniskunjungan'],
                                                 'estimasidilayani'           => '',
                                                 'kodedokter'           => (isset($value['kodedokter']))?$value['kodedokter']:'',
                                                 'namadokter'           => $data_dokter_ol->nama_dokter,
                                                 'tanggaloperasi'           => '',
                                                 'jenistindakan'           => '',
                                                 'islayani'           => '',
                                            );
                                            $insert = $this->db->insert('pasien_daftar', $data_);
                                            
                                            $jam_ex = explode("-",$this->post("jampraktek"));
                                            $this->db->where('p.nomorkartu', $this->post("nomorkartu"));
                                            $this->db->where('p.nik', $this->post("nik"));
                                            $this->db->where('p.notelp', $this->post("nohp"));
                                            $this->db->where('p.kodepoli', $this->post("kodepoli"));
                                            $this->db->where('p.tanggalperiksa', $this->post("tanggalperiksa"));
                                            $this->db->where('s.kodedokter', $this->post("kodedokter"));
                                            $this->db->order_by('p.tanggalperiksa', 'DESC');
                                            $this->db->select('p.nomorantrian as nomorantrean, p.nomorantrian as angkaantrean, p.kodebooking, "1" as pasienbaru, p.norm, p.namapoli, p.namadokter, p.estimasidilayani, s.sisakuotajkn, s.kuotanonjkn, s.sisakuotanonjkn, s.kuotanonjkn, s.keterangan');
                                            $this->db->from('pasien_daftar p');
                                            $this->db->join('status_antrean s', 's.kodepoli = p.kodepoli');
                    
                                            $kontak = $this->db->get()->result();
                                            
                                            $id = count($kontak);
                            
                                            if ($id <= 0)
                                            {
                                                //$data['response'] = array( 'status' => FALSE, 'message' => 'Tidak ada data'); 
                                                $data['metadata'] = array('message' => "data tidak ada", "code" => 200 );
                                                $this->response($data, REST_Controller::HTTP_OK); // BAD_REQUEST (400) being the HTTP response code
                                            }else{
                                                $data['response'] = $kontak; 
                                                $data['metadata'] = array('message' => "OK", "code" => 200 );
                                                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                                            }    
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private function cek_tanggal($date)
    {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            $Date1 = strtotime(date('Y-m-d', strtotime($date) ) ).' ';
            $Date2 = strtotime(date('Y-m-d'));
            
             if($Date1 < $Date2 OR $Date1 > $Date2) {
                return 0;
            }else{
                 return 1;
            }
        } else {
            return 2;
        }
    }
    
    private function encode_jwt($payload)
    {
        $key = JWT::key_encode_decode();
        $jwt = JWT::encode($payload, $key);
        return $jwt;
    }

    private function decode_jwt($jwt)
    {
        $key = JWT::key_encode_decode();
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        return $decoded;
    }

    function index_get() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_put() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_delete() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
}
