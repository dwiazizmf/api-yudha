<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Pasien_daftar extends REST_Controller {
	function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        
        $this->load->database();
    }

    public function index_post()
    {
        if ( ($this->post('data') === NULL ||  $this->post('data') === ''))
        {
            $data['response'] = array( 'status' => FALSE, 'message' => 'data no pendaftaran tidak dikirim'); 
            $data['metadata'] = array('message' => FALSE, "code" => 405 );
            $this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED); // NOT_FOUND (404) being the HTTP response code
        }else{
            $datanya = $this->post('data');
            
            foreach ($datanya as $value) {
               if ( ($value['nopendaftaran'] === NULL ||  $value['nopendaftaran'] === ''))
                {
                    $data['response'] = array( 'status' => FALSE, 'message' => 'data no pendaftaran tidak dikirim'); 
                    $data['metadata'] = array('message' => FALSE, "code" => 405 );
                    $this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED); // NOT_FOUND (404) being the HTTP response code
                }else {
                    $this->db->where("nopendaftaran", $value["nopendaftaran"]);
                    $this->db->select('nopendaftaran');
                    $pasien_daftar = $this->db->get('pasien_daftar')->result();
                    if(count($pasien_daftar) > 0){
                        $nopendaftaran = $value['nopendaftaran'];
                        $this->db->where('nopendaftaran', $nopendaftaran);
                        $delete = $this->db->delete('pasien_daftar');
                    }else{
                        $this->db->where("kodepoli", $value["kodepoli"]);
                        $this->db->where("namapoli", $value["namapoli"]);
                        $this->db->where("namadokter", $value["namadokter"]);
                        $this->db->where("tanggalperiksa", $value["tanggalperiksa"]);
                        $this->db->select('id, totalantrean, sisaantrean, kuotanonjkn, sisakuotanonjkn');
                        $statusAntrean = $this->db->get('status_antrean')->result();
                        //var_dump($statusAntrean);
                        $id = count($statusAntrean);
                        if($id > 0){
                            foreach($statusAntrean as $dataStatusAntrean){
                                //var_dump($dataStatusAntrean->sisaantrean);
                                $sisaAntrean = $dataStatusAntrean->sisaantrean - 1;
                                
                                //var_dump($sisaAntrean);
                                $this->db->where('id', $dataStatusAntrean->id);
                                $this->db->update('status_antrean', array('sisaantrean' => $sisaAntrean, 'sisakuotanonjkn' => $sisaAntrean));
                            }
                        }else{
                            $this->db->where("tgl_booking", $value["tanggalperiksa"]);
                            $this->db->where('kd_unit', $value["kodepoli"]);
                            $this->db->select('nid');
                            $pendaftaran_ol = $this->db->get('pendaftaran_ol')->result();
                            $jml_pendaftaran_ol = count($pendaftaran_ol);
                            //var_dump($pendaftaran_ol);
                            $this->db->where("nama_dokter", $value["namadokter"]);
                            $this->db->select('kd_dokter');
                            $dokter_ol = $this->db->get('dokter_ol')->result();
                            $data_dokter_ol = $dokter_ol[0];
                            
                            $data_antrean = array(
                                'kodepoli'           => $value['kodepoli'],
                                 'namapoli'           => $value['namapoli'],
                                 'kodedokter'           => $data_dokter_ol->kd_dokter,
                                 'namadokter'           => $value['namadokter'],
                                 'tanggalperiksa'           => $value['tanggalperiksa'],
                                 'jampraktek_start'           => '08:00',
                                 'jamprakter_end'           => '16:00',
                                 'totalantrean'           => 100,
                                 'sisaantrean'           => 99 - $jml_pendaftaran_ol,
                                 'antreanpanggil'           => 0,
                                 'kuotajkn'           => 0,
                                 'sisakuotajkn'           => 0,
                                 'kuotanonjkn'           => 100,
                                 'sisakuotanonjkn'           => 99 - $jml_pendaftaran_ol,
                                 'keterangan'           => '',
                            );
                            $insert = $this->db->insert('status_antrean', $data_antrean);
                        }
                    }
                    
                    $this->insertData($value, $data_dokter_ol->kd_dokter);
                }  
            }
        }         
    }
    
    private function insertData($value, $kd_dokter){
        
            
            
            $data_ = array(
                    'nopendaftaran'           => $value['nopendaftaran'],
                     'nomorkartu'           => $value['nomorkartu'],
                     'nik'           => $value['nik'],
                     'notelp'           => $value['notelp'],
                     'tanggalperiksa'           => $value['tanggalperiksa'],
                     'kodepoli'           => $value['kodepoli'],
                     'namapoli'           => $value['namapoli'],
                     'nomorreferensi'           => $value['nomorreferensi'],
                     'jenisreferensi'           => $value['jenisreferensi'],
                     'jenisrequest'           => $value['jenisrequest'],
                     'polieksekutif'           => $value['polieksekutif'],
                     'nomorantrian'           => $value['nomorantrian'],
                     'kodebooking'           => $value['kodebooking'],
                     'jenisantrian'           => $value['jenisantrian'],
                     'estimasidilayani'           => $value['estimasidilayani'],
                     'kodedokter'           => $kd_dokter,
                     'namadokter'           => $value['namadokter'],
                     'tanggaloperasi'           => $value['tanggaloperasi'],
                     'jenistindakan'           => $value['jenistindakan'],
                     'islayani'           => $value['islayani'],
                );
            $insert = $this->db->insert('pasien_daftar', $data_);
            if ($insert) {
              $data['response'] = array( 'status' => TRUE, 'message' => 'Berhasil tambah data'); 
                $data['metadata'] = array('message' => "OK", "code" => 200 );
                //$this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                $this->response($data, 200);
            } else {
                $data['response'] = array( 'status' => FALSE, 'message' => 'Bad Gateway'); 
                $data['metadata'] = array('message' => FALSE, "code" => 502 );
                $this->set_response($data, REST_Controller::HTTP_BAD_GATEWAY );
            }
    }
    
    function index_get() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Tidak ada method get untuk service ini'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_put() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Tidak ada method get untuk service ini'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_delete() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Tidak ada method get untuk service ini'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
}
