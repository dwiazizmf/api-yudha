<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/jwt/JWT.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class sekolah extends REST_Controller {
    
    private $newdb;
	
    function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        
        $this->newdb = $this->load->database('default',TRUE);
    }

    public function index_post()
    {
     	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus GET'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    
    }

    

    function index_get() {
        // //phpinfo();
        

        //var_dump("expression");
        // $kontak = $this->newdb->query("SELECT distinct s.kabupaten,ks.kode_kategori,ks.kategori
        //                     from sekolah s
        //                     join kategori_sekolah ks on ks.oid=s.kategori_sekolah
        //                     where s.aktif=1 ")->result();
        $kode_kategori = htmlentities(urldecode($this->get('kd_kat')));
        $kode_kabupaten = htmlentities(urldecode($this->get('kd_kab')));


        $kontak = $this->newdb->query("SELECT distinct s.kode,s.nama,s.url,email,npsn,nama_operator,tlp_operator,s.kode_kabupaten,kabupaten,
                                kecamatan,kelurahan,kepala_sekolah,tlp_kepala_sekolah,email_kepala_sekolah,jml_guru,jml_siswa,nspmnu,ks.kode_kategori,ks.kategori
                                from sekolah s
                                join kategori_sekolah ks on ks.oid=s.kategori_sekolah
                                where s.aktif=1 and ks.kode_kategori= '$kode_kategori' and s.kode_kabupaten= '$kode_kabupaten' ")->result();
        

                    $data['response'] = $kontak; 
                    $data['metadata'] = array('message' => "OK", "code" => 200 );
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

    }

    function index_put() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function index_delete() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus POST'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }
}
