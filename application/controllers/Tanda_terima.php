<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/jwt/JWT.php';

// use namespace
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Tanda_terima extends REST_Controller {
    
    private $newdb;
    private $query;
	
    function __construct($config = 'rest') {
        
        parent::__construct($config);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        
        $this->newdb = $this->load->database('default',TRUE);
        $this->query = "SELECT 
                        Connote.ConnoteNumber,  
                        CASE
                            WHEN Connote.FlagFullContainer = 1 
                                THEN Connote.PickupTime
                            ELSE
                                Connote.ConnoteDate
                        END AS ConnoteDate,
                        ShipFrom.ShipFromName,
                        ShipFrom.Address1,
                        REPLACE(Item.Name,CHAR(9),'') AS ItemName,
                        Item.Description AS ItemDescription,
                        ConnoteItem.QTY,
                        UOM.Code AS UOM,
                        ContainerBooking.ContainerNumber,
                        ContainerBooking.SealNumber,
                        ContainerBooking.BookingDate,
                        Ship.Code AS ShipCode,
                        STR(ConnoteItem.WeightVol, 25, 3) as WeightVol,STR(ConnoteItem.Weight, 25, 3) as Weight,st.ServiceTypeCode,ManifestDetail.SealNumber as sealNo,ContainerNumber,ct.PackageTypeCode,
                        ShipFrom.ContactPhone,ShipFrom.ContactName,
                        s.ShipFromName as consigneeName,isnull(dbo.GetMerk(Connote.ConnoteNumber),CustomerProjectNumber) as CustomerProjectNumber,
                        s.Address1 as consigneeAddress,s.ContactPhone as consigneePhone,s.ContactName as consigneeContact,ConnoteItem.OID,
                        cr.Name as pt,Connote.PackageDescription,ConnoteItem.Width,ConnoteItem.Length,ConnoteItem.Height
                        ,Item.Code as itemCode ,
                        case when ConnoteItem.Height>0 then cast(format(ConnoteItem.Length,'N0') as varchar) + ' x ' + cast(format(ConnoteItem.Width,'N0') as varchar) 
                        + ' x ' + cast(format(ConnoteItem.Height,'N0') as varchar) + ' x ' + cast(format(ConnoteItem.QTY,'N0') as varchar)
                        when ConnoteItem.Weight_qty>0 then cast(format(ConnoteItem.Weight_qty,'N0') as varchar) 
                        + ' x ' + cast(format(ConnoteItem.QTY,'N0') as varchar )else '' end  as ukuran,replace(REPLACE(Item.Name,CHAR(9),'') + REPLACE(UOM.code,CHAR(9),''),' ' ,'')  as grop, st.ServiceTypeName, (case when(ct2.Name != '') then ct2.Name else ct.PackageTypeCode end) as CtName
                        FROM Connote
                        join CustomerProject cp on cp.OID=Connote.CustomerProject
                            join Corporate cr on cr.OID=cp.Corporate
                        LEFT JOIN ShipFrom ON ShipFrom.OID = Connote.OriginLocation
                        LEFT JOIN ShipFrom s ON s.OID = Connote.DestinationLocation
                        LEFT JOIN ConnoteItem ON  ConnoteItem.Connote = Connote.OID
                        LEFT JOIN ContainerBooking ON  ContainerBooking.OID = Connote.ContainerBooking
                        LEFT JOIN Item ON  Item.OID = ConnoteItem.Item
                        LEFT JOIN UOM ON UOM.OID = ConnoteItem.UOM
                        LEFT JOIN ManifestDetail ON ManifestDetail.Oid = Connote.ManifestDetail
                        LEFT JOIN Manifest ON Manifest.Oid = ManifestDetail.Manifest
                        LEFT JOIN Ship ON  Ship.OID = Manifest.Ship
                        inner join ServiceType st on st.OID=Connote.PackageServiceType
                        left join PackageType ct on ct.OID=Connote.PackageType
                        left join ContainerType ct2 on Connote.ContainerType=ct2.OID";
    }

    function print_post() {
        $data['response'] = array( 'status' => FALSE, 'message' => 'Method harus GET'); 
        $data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function print_put() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus GET'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function print_delete() {
    	$data['response'] = array( 'status' => FALSE, 'message' => 'Method harus GET'); 
    	$data['metadata'] = array('message' => FALSE, "code" => 404 );
        $this->response($data, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
    }

    function print_get()
    {
     	try {
            //var_dump("expression");
            $where = htmlentities(urldecode($this->get('connote')));


            $kueri = $this->query;
            $kueri .= "   WHERE Manifest.GCRecord IS NULL AND ManifestDetail.GCRecord IS NULL
                                            and Connote.ConnoteNumber in ('".$where."') order by Item.Name, UOM.Code";

            $result = $this->newdb->query($kueri)->result();
            
            $this->load->view("print_kosong",array('data_tt' => $result));
            //var_dump($result);
            // $data['data'] = $result; 
            // $data['status'] = 'SUCCESS'; 
            // $data['metadata'] = array('message' => "OK", "code" => 200 );
            // $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code 
        } catch (\Exception $e) {
            $data['status'] = 'FAILED'; 
            $data['metadata'] = array('message' => "Operation Failed", "code" => 500 );
            $this->set_response($data, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // OK (200) being the HTTP response code
        }
    }

    function view_get() {
        try {
            //var_dump("expression");
            $where = htmlentities(urldecode($this->get('connote')));

            $kueri = $this->query;
            $kueri .= "   WHERE Manifest.GCRecord IS NULL AND ManifestDetail.GCRecord IS NULL
                                            and Connote.ConnoteNumber in ('".$where."') order by Item.Name, UOM.Code";

            $result = $this->newdb->query($kueri)->result();
            
            $this->load->view("print",array('data_tt' => $result));
            //var_dump($result);
            // $data['data'] = $result; 
            // $data['status'] = 'SUCCESS'; 
            // $data['metadata'] = array('message' => "OK", "code" => 200 );
            // $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code 
        } catch (\Exception $e) {
            $data['status'] = 'FAILED'; 
            $data['metadata'] = array('message' => "Operation Failed", "code" => 500 );
            $this->set_response($data, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // OK (200) being the HTTP response code
        }
    }

    function tes_get()
    {
        $this->tes = $this->load->database('default',TRUE);

        try {
            //var_dump("expression");
            $where = htmlentities(urldecode($this->get('connote')));

            $kueri = $this->query;
            $kueri .= "   WHERE Manifest.GCRecord IS NULL AND ManifestDetail.GCRecord IS NULL
                                            and Connote.ConnoteNumber in ('".$where."') order by Item.Name, UOM.Code";

            $result = $this->tes->query($kueri)->result();

            // echo "<pre>";
            // var_dump($result);
            // echo "</pre>";
            
            $this->load->view("print_tes",array('data_tt' => $result));
            //var_dump($result);
            // $data['data'] = $result; 
            // $data['status'] = 'SUCCESS'; 
            // $data['metadata'] = array('message' => "OK", "code" => 200 );
            // $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code 
        } catch (\Exception $e) {
            $data['status'] = 'FAILED'; 
            $data['metadata'] = array('message' => "Operation Failed", "code" => 500 );
            $this->set_response($data, REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // OK (200) being the HTTP response code
        }
    }
}
